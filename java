 By default, Ubuntu 18.04 includes Open JDK, which is an open-source variant of the JRE and JDK.

sudo apt update

Next, check if Java is already installed:
java -version

If Java is not currently installed, you’ll see the following output:

Output
Command 'java' not found, but can be installed with:

apt install default-jre
apt install openjdk-11-jre-headless
apt install openjdk-8-jre-headless
apt install openjdk-9-jre-headless


Install openjdk
sudo apt install default-jre
sudo apt install default-jdk

.
Installing Specific Versions of OpenJDK
sudo apt install openjdk-8-jdk
sudo apt install openjdk-11-jdk

Installing the Oracle JDK

sudo add-apt-repository ppa:linuxuprising/java
sudo apt update
sudo apt-get install oracle-java12-installer

Managing Java
sudo update-alternatives --config java


Setting the JAVA_HOME Environment Variable
Many programs written using Java use the JAVA_HOME environment variable to determine the Java installation location.

sudo update-alternatives --config java
sudo nano /etc/environment
JAVA_HOME="/usr/lib/jvm/java-11-openjdk-amd64/bin/"
source /etc/environment
echo $JAVA_HOME

Manually install java 
sudo mkdir /opt/java-jdk
sudo tar -C /opt/java-jdk -zxf ~/Downloads/jdk-13_linux-x64_bin.tar.gz
sudo update-alternatives --install /usr/bin/java java /opt/java-jdk/jdk-13/bin/java 1
sudo update-alternatives --install /usr/bin/javac javac /opt/java-jdk/jdk-13/bin/javac 1

 sudo update-alternatives --config java

