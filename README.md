# gnu-linux-class-plan

## Histroy 
 - GNU
 - Linux
 - Debian
 - Ubuntu - Flavours 
 - Theng os 
 - Desktop Environments & Xorg 
 

## Installation 
 - Bootable Software
   - Rufus
   - Gnome Disk
   - DD
   - Startup Disk Creator 

 - Partitons explained 
   - / , /home , /swap
 
 - UEFI/Legacy
   - Installing Ubuntu 
   - Installing Debian 
   - Dualboot Ubuntu & Debian
   - MBR/GPT
   - Dualbooting with Windows 

 - How Linux Boots 

 - Boot Issues
   - Boot repair utility 
   - Other boot issues 

## Ubuntu  
 - Ubuntu Desktop intro and common applications 
 - Root partiton (/) explained 
 - Intro to terminal & commands 
 - Software Installation
   - Software center
   - APT
     - Package Manager explained Need & use 
     - Apt update & Apt upgrade 
     - repo location 
     - 
     
   - Synaptic 
   - Source Installation 
   - Manual Installation 

## User Group & Permissions 
 - Creating , deleting & modifying users
 - Passwd file explained 
 - Change password 
 - Fix password errors & boot break 
 - ls command output explained 
 - Advanced permissions 
   - Scenario walkthrough

## Networking 
 - Intro to Networking 
 - Setup static IP
 - How Dns works 
   - resolv.conf
   - hosts file 
 - How DHCP works 
   - Setup a DHCP server 

## Webservers
 - Installing and configuring nginx
 - Adding encryption with SSL/TLS
 - Securing nginx's configuration file
 - Blocking malicious activity with Fail2ban

## Virtualization 
 - What ? why ?
 - Classification 
  - Bare metal 
  - Hosted
 - Hypervisors 
  - Virtual Box
  - KVM
 - OS Installation with in Hypervisors 
 - SSH into Vms 
 
## Remote Administration
 - SSH
  - setup SSH
  - change default port 
  - keybased login  

## File Sharing 
 - SFTP
  -Jail users
 - NFS
   - Setup NFS share 
 - SAMBA
   - setup samba server for file share 

## Backup 
 -  
## Network Booting 
 -Intro to PXE 

## Shell Scripting 
- 

