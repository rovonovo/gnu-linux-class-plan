Set /home as the Chroot directory.

# create a group for SFTP
root@dlp:~# groupadd sftp_users

# apply to a user "[ubuntu] for SFTP only as an example
root@dlp:~# usermod -G sftp_users ubuntu

root@dlp:~# vi /etc/ssh/sshd_config

# line 115: comment out and add a line like below

#Subsystem sftp /usr/lib/openssh/sftp-server

Subsystem sftp internal-sftp

# add to the end
Match Group sftp_users
  ChrootDirectory /home
  ForceCommand internal-sftp

root@dlp:~# systemctl restart ssh 

In the above:
    Match Group sftpusers – This indicates that the following lines will be matched only for users who belong to group sftpusers

    ChrootDirectory /sftp/%u – This is the path that will be used for chroot after the user is authenticated. %u indicates the user. So, for john, this will be /sftp/john.

    ForceCommand internal-sftp – This forces the execution of the internal-sftp and ignores any command that are mentioned in the ~/.ssh/rc file.


## custom folder 
