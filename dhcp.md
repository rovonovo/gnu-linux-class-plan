# DHCP

## Installing ISC DHCP Server:

$ sudo apt update
$ sudo apt install isc-dhcp-server

## ref

enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_co
    link/ether 52:54:00:f9:2a:bb brd ff:ff:ff:ff:ff:ff
    inet 192.168.122.29/24 brd 192.168.122.255 scope global dynam

## setup Interface

sudo nano /etc/default/isc-dhcp-server 
INTERFACESv4="enp1s0"

## Main config 

sudo nano /etc/dhcp/dhcpd.conf

If this DHCP server is the official DHCP server for the local
# network, the authoritative directive should be uncommented
authoritative

# A slightly different configuration for an internal subnet.
subnet 192.168.122.0 netmask 255.255.255.0 {
  range 192.168.122.80 192.168.122.90;
#  option domain-name-servers ns1.internal.example.org;
#  option domain-name "internal.example.org";
  option subnet-mask 255.255.255.0;
  option routers 192.168.122.255;
  option broadcast-address 192.168.122.255;
  default-lease-time 600;
  max-lease-time 7200;
}

Assigning Fixed IP Addresses to Hosts via DHCP

host rz {
  hardware ethernet 52:54:00:19:7b:dc;
  fixed-address 192.168.122.85;
}

sudo systemctl restart isc-dhcp-server
